var paypalRefreshToken = false;
/**
 * Paypal service
 */

var app = app || {};



app.Paypal = {

 subscribeWithPayPal: function(successCallback, errorCallback) {    
    var clientIDs = {
        "PayPalEnvironmentProduction": "YOUR_PRODUCTION_CLIENT_ID",
        "PayPalEnvironmentSandbox": appSettings.paypal.clientId
    };
    PayPalMobile.init(clientIDs, function () {
        if(appSettings.debug) alert('paypal init');
        // onPayPalMobileInit
        PayPalMobile.prepareToRender(appSettings.paypal.environment, new PayPalConfiguration({
             merchantName: appSettings.paypal.merchantName,
             merchantPrivacyPolicyURL: appSettings.paypal.merchantPrivacyPolicyURL,
             merchantUserAgreementURL: appSettings.paypal.merchantUserAgreementURL,
             defaultUserEmail: appSettings.paypal.defaultUserEmail,
             sandboxUserPassword: appSettings.paypal.sandboxUserPassword
         }), function () {
             // onPrepareRender
             PayPalMobile.renderFuturePaymentUI(function (authorization) {
                 // onFuturePaymentAuthorization
                 if(appSettings.debug) alert('Authorization succeeded: ' + JSON.stringify(authorization));
                 var authCode = authorization.response.code;
                 $.ajax({
                            type: 'POST',
                            url: appSettings.paypal.APIbaseURL + '/v1/oauth2/token',
                            headers: {
                         'Content-Type': 'application/x-www-form-urlencoded',
                         'Authorization': 'Basic ' + btoa(appSettings.paypal.clientId + ':' + appSettings.paypal.secret)
                     },
                            data: 'grant_type=authorization_code&response_type=token&redirect_uri=urn:ietf:wg:oauth:2.0:oob&code=' + authCode,
                            success: function (data) {
                                paypalRefreshToken = data.refresh_token;
                                if(appSettings.debug) alert('Get refresh token succeeded');
                                console.log('Get refresh token succeeded');
                                successCallback();
                            },
                            error: function (error) {
                                if(appSettings.debug) alert('Get refresh token failed' + JSON.stringify(error));
                                console.log('Get refresh token failed' + JSON.stringify(error));
                                errorCallback();
                            }
                        });
             }, function () {
                 // onUserCanceled
                 if(appSettings.debug) alert('onUserCanceled');
                 console.log('onUserCanceled');
                 errorCallback();
             });
         });
    });
},
 executePayPalPayment: function() {
    if(appSettings.debug) alert('executePayPalPayment');
    startLoading();
                
    PayPalMobile.applicationCorrelationIDForEnvironment(appSettings.paypal.environment, function(data) {
        if(appSettings.debug) alert('applicationCorrelationIDForEnvironment data: ' + JSON.stringify(data));
        var paypalApplicationCorrelationId = data;
        
        //TODO: get persisted user's choise about currency
        var currency = 'USD';
        $.ajax({
           type: 'POST',
           url: appSettings.paypal.APIbaseURL + '/v1/oauth2/token',
           headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'Basic ' + btoa(appSettings.paypal.clientId + ':' + appSettings.paypal.secret)
           },
           data: 'grant_type=refresh_token&refresh_token=' + paypalRefreshToken,
           success: function (data) {
               if(appSettings.debug) alert('PayPal refresh access token succeeded: ');
               var accessToken = data.access_token;
               // Capture payment now with access token
               var paymentObj = {
                   intent: 'sale',
                   payer: {
                       payment_method: "paypal"
                   },
                   transactions: [{
                       "amount": {
                           "total": "1.00",
                           "currency": currency,
                           "details": {
                               "subtotal": "1.00",
                               "tax": "0.00",
                               "shipping": "0.00"
                           }
                       },
                       "description": "This is the future payment transaction description."
                   }]
               };

               $.ajax({
                  type: 'POST',
                  url: 'https://api.sandbox.paypal.com/v1/payments/payment',
                  headers: {
                   'Content-Type': 'application/json',
                   'Authorization': 'Bearer ' + accessToken,
                   'Paypal-Application-Correlation-Id': paypalApplicationCorrelationId
               },
                  data: JSON.stringify(paymentObj),
                  success: function (data) {
                      if(appSettings.debug) alert('You (' + data.payer.payer_info.first_name + ' ' + data.payer.payer_info.last_name + ') payed ' + data.transactions[0].amount.total + ' ' + data.transactions[0].amount.currency);
                      //hideLoader();
                  },
                  error: function (error) {
                      if(appSettings.debug) alert('PayPal finishing payment error: ' + JSON.stringify(error));
                      //hideLoader();
                  }
              });
           },
           error: function (error) {
               if(appSettings.debug) alert('PayPal refresh access token error: ' + JSON.stringify(error));
               //hideLoader();
           }
       });
    });
}
};