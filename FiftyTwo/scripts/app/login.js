/**
 * Login view model
 */

var app = app || {};

app.Login = (function () {
    'use strict';
    
    var loginViewModel = (function () {
        var currentUser = kendo.observable({ data: null });
        var usersData;

        // Retrieve current user and all users data from Backend Services
        var loadUsers = function () {
            // Get the data about the currently logged in user
            return app.everlive.Users.currentUser()
                .then(function (data) {
                    var currentUserData = data.result;
                    //currentUserData.PictureUrl = app.helper.resolveProfilePictureUrl(currentUserData.Picture);
                    currentUser.set('data', currentUserData);

                    // Get the data about all registered users
                    return app.everlive.Users.get();
                })
                .then(function (data) {
                    usersData = new kendo.data.ObservableArray(data.result);
                })
                .then(null, function (err) {
                          app.showError(err.message);
                      }
                    );
        };
        
        var getUser = function(id) {
           el.Users.getById(id)
                .then(function (data) {
                    return data;
                },
                function(err){
                    app.showError(err.message);
                });
        };
        
        // Authenticate to use Backend Services as a particular user
        var login = function () {
            app.startLoading();

            var username = $('#login-email').val();
            var password = $('#login-password').val();

            // Authenticate using the username and password
            app.everlive.Users.login(username, password, function(data) {
                console.log('Logged in user: '+JSON.stringify(data));
                app.userId = data.result.principal_id;
                app.everlive.Users.currentUser()
                    .then(function (data) {
                        app.currentUser = data.result;
                        app.hideLoader();
                        app.mobileApp.navigate('views/campaignsView.html');
                        $('#login-email').val('');
                        $('#login-password').val('');
                    },
                    function(error){
                        app.hideLoader();
                        alert(JSON.stringify(error));
                    });
                
            }, function(error) {
                console.log('Logging error: '+JSON.stringify(error));
                app.hideLoader();
                alert(error.message);
            });
        };
        var logout = function () {
            app.everlive.Users.logout();
            app.currentUser = null;
            app.userId = null;
            app.mobileApp.navigate('views/appEntrance.html');
        };

        // Authenticate using Facebook credentials
        var loginWithFacebook = function() {

            if (!isFacebookLogin) {
                return;
            }
            if (isInMistSimulator) {
                showMistAlert();
                return;
            }
            var facebookConfig = {
                name: 'Facebook',
                loginMethodName: 'loginWithFacebook',
                endpoint: 'https://www.facebook.com/dialog/oauth',
                response_type: 'token',
                client_id: appSettings.facebook.appId,
                redirect_uri: appSettings.facebook.redirectUri,
                access_type: 'online',
                scope: 'email',
                display: 'touch'
            };
            var facebook = new IdentityProvider(facebookConfig);
            app.mobileApp.showLoading();

            facebook.getAccessToken(function(token) {
                app.everlive.Users.loginWithFacebook(token)
                .then(function () {
                    // EQATEC analytics monitor - track login type
                    if (isAnalytics) {
                        analytics.TrackFeature('Login.Facebook');
                    }
                    return app.Users.load();
                })
                .then(function () {
                    app.mobileApp.hideLoading();
                    app.mobileApp.navigate('views/activitiesView.html');
                })
                .then(null, function (err) {
                    app.mobileApp.hideLoading();
                    if (err.code === 214) {
                        app.showError('The specified identity provider is not enabled in the backend portal.');
                    } else {
                        app.showError(err.message);
                    }
                });
            });
        };

        var loginWithGoogle = function () {

            if (!isGoogleLogin) {
                return;
            }
            if (isInMistSimulator) {
                showMistAlert();
                return;
            }
            var googleConfig = {
                name: 'Google',
                loginMethodName: 'loginWithGoogle',
                endpoint: 'https://accounts.google.com/o/oauth2/auth',
                response_type: 'token',
                client_id: appSettings.google.clientId,
                redirect_uri: appSettings.google.redirectUri,
                scope: 'https://www.googleapis.com/auth/userinfo.profile',
                access_type: 'online',
                display: 'touch'
            };
            var google = new IdentityProvider(googleConfig);
            app.mobileApp.showLoading();

            google.getAccessToken(function(token) {
                app.everlive.Users.loginWithGoogle(token)
                .then(function () {
                    // EQATEC analytics monitor - track login type
                    if (isAnalytics) {
                        analytics.TrackFeature('Login.Google');
                    }
                    return app.Users.load();
                })
                .then(function () {
                    app.mobileApp.hideLoading();
                    app.mobileApp.navigate('views/activitiesView.html');
                })
                .then(null, function (err) {
                    app.mobileApp.hideLoading();
                    if (err.code === 214) {
                        app.showError('The specified identity provider is not enabled in the backend portal.');
                    } else {
                        app.showError(err.message);
                    }
                });
            });
        };

        return {
            load: loadUsers,
            loginWithGoogle: loginWithGoogle,
            loginWithFacebook: loginWithFacebook,
            login: login,
            logout: logout,
            getUser: getUser,
            users: function () {
                return usersData;
            },
            currentUser: currentUser
        };
    }());

    return loginViewModel;
}());