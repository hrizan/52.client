/**
 * news view model
 */

var app = app || {};

app.News = (function (kendo) {
    var newsVM = function() {
        this.newsModel = {
            id: 'Id',
            fields: {
                Description: {
                    field: 'Description',
                    defaultValue: ''
                },
                Title: {
                    field: 'Title',
                    defaultValue: ''
                },
                CreatedAt: {
                    field: 'CreatedAt',
                    defaultValue: new Date()
                }
            }
        };
        
        this.newsDataSource = new kendo.data.DataSource({
            type: 'everlive',
            schema: {
                model: this.newsModel,
                parse: function (response) {
                    var s = "";
                    var result = [];
                    $.each(response.Result, function (index, item) {
                        item.CreatedAtString = kendo.toString(item.CreatedAt, 'MMM d, yyyy');
                        result.push(item);
                    });
                    return result;
                }
            },
            transport: {
                // Required by Backend Services
                typeName: 'News'
            },
            change: function (e) {

            },
            sort: { field: 'CreatedAt', dir: 'desc' }
        });
        
        var viewModel = kendo.observable(this);

        return viewModel;
    }
    
    var model = new newsVM();
    
    return {
        model: model
    };
}(kendo));
