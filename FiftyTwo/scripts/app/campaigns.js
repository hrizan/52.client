/**
 * Organizations view model
 */

var app = app || {};

kendo.data.binders.voteClass = kendo.data.Binder.extend({
        refresh: function() {
            var value = this.bindings["voteClass"].get();

            if (value) {
                $(this.element).removeClass('campaign-details-logo');
                $(this.element).addClass('campaign-details-logo-voted');
            } else {
                $(this.element).removeClass('campaign-details-logo-voted');
                $(this.element).addClass('campaign-details-logo');
            }
        }
    });

app.Campaigns = (function () {
    'use strict'
    
    // organizations model
    var campaignsModel = (function () {
        var campaignModel = {

            id: 'Id',
            fields: {
                Name: {
                        field: 'Name',
                        defaultValue: ''
                    },
                Description: {
                        field: 'Description',
                        defaultValue: ''
                    },
                CreatedAt: {
                        field: 'CreatedAt',
                        defaultValue: new Date()
                    },
                PictureSmall: {
                        fields: 'PictureSmall',
                        defaultValue: null
                    },  
                PictureBig: {
                        fields: 'PictureBig',
                        defaultValue: null
                    },
                OrganizationId: {
                        field: 'OrganizationId',
                        defaultValue: ''
                    },
                NrVotes: {
                        field: 'NrVotes',
                        defaultValue: 0
                    },
                Organization: {
                        field: 'Organization',
                        defaultValue: ''
                    }
            },
            CreatedAtFormatted: function () {
                return app.AppHelper.formatDate(this.get('CreatedAt'));
            },
            GetPictureSmall: function () {
                if (this.PictureSmall) {
                    return app.AppHelper.resolvePictureUrl(this.get('PictureSmall'));
                }
            },
            HasVoted: function(){
                if(app.currentUser && app.currentUser.HasVoted && this.Id === app.currentUser.CampaignId){
                    return true;
                }
                
                return false;
            }
        };
        
        var expandExpr = {
            "OrganizationId": true
        };

        // organizations data source. The Backend Services dialect of the Kendo UI DataSource component
        // supports filtering, sorting, paging, and CRUD operations.
        var campaignsDataSource = new kendo.data.DataSource({
            type: 'everlive',
            schema: {
                model: campaignModel
            },
            transport: {
            // Required by Backend Services
            typeName: 'Campaign',                
            read: {
                contentType: "application/json",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("X-Everlive-Expand", JSON.stringify(expandExpr));
                }
                }
                },
                sort: { field: 'NrVotes', dir: 'desc' }
            });        

        return {
            campaigns: campaignsDataSource
        };
    }());

    // organizations view model
    var campaignsViewModel = (function () {        
        var showRefresh = false;
        
        var show = function (e) {
            if(app.Campaigns.showRefresh === true){
                    app.Campaigns.showRefresh = false;
                    app.Campaigns.campaigns.read();
                }
            };
        
        var addVote = function (e) {          
            var campaign = e.data;
            var data = app.everlive.data('Campaign');
            var userId = app.userId;
            
             if(app.currentUser.HasVoted) {
                alert('You already voted this week');
                 return;
            }
            
            app.startLoading();
            //e.sender.element.removeClass('campaign-details-logo');
            //e.sender.element.addClass('campaign-details-logo-voted');
            //this.set('NrVotes', campaign.NrVotes + 1);
            data.updateSingle({ Id: campaign.Id, 'NrVotes': campaign.NrVotes + 1 },
                function(data){
                    app.everlive.Users.updateSingle({ 'Id': userId, 'HasVoted': true, 'CampaignId': campaign.Id },
                    function(data){
                        //show modalBox
                        app.currentUser.HasVoted = true;
                        app.currentUser.CampaignId = campaign.Id;
                        app.hideLoader();
                        app.Campaigns.campaigns.read();
                        openDetails(data);
                    },
                    function(error){
                        app.hideLoader();
                        app.showError(error.message);
                        app.mobileApp.navigate('views/appEntrance.html');
                    });
                },
                function(error){
                    app.hideLoader();
                    app.showError(error.message);
                    app.mobileApp.navigate('views/appEntrance.html');
                });              
        };

        function openDetails(data) {
                $("#votedModal").data("kendoMobileModalView").open();
            }
        
        var checkSimulator = function (){
            if (window.navigator.simulator === true) {
                alert('This plugin is not available in the simulator.');
                return true;
            } else if (window.plugins === undefined || window.plugins.socialsharing === undefined) {
                alert('Plugin not found. Maybe you are running in AppBuilder Companion app which currently does not support this plugin.');
                return true;
            } else {
                return false;
            }
        };
        
        var shareImagesViaFacebook = function () {
            if (!checkSimulator()) {
                // For the files param you can pass null, a single string or an array.
                window.plugins.socialsharing.shareViaFacebook('I just voted on FiftyTwo app', null, 'https://www.facebook.com/befiftytwo', 
                function() {console.log('share ok')}, 
                function(error){app.showError(error.message)});
            }
        };
        
        return {
            showRefresh:showRefresh,
            show: show,
            shareImagesViaFacebook: shareImagesViaFacebook,
            checkSimulator: checkSimulator,
            campaigns: campaignsModel.campaigns,
            addVote: addVote
        };
    }());

    return campaignsViewModel;
}());