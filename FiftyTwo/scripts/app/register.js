/**
 * Register view model
 */

var app = app || {};

app.Register = (function () {
    'use strict';
    
    var registerViewModel = (function () {
        var registerModel = kendo.observable({
                                                 Username: '',
                                                 Password: '',
                                                 ConfirmPassword: '',
                                                 PaymentMethod: 'paypal',
                                                 Currency: 'USD',
                                                 isMail: true,
                                                 isPayment: false,
                                                 selectedGroup: 'mail',
                                                 termsAccepted: false,
                                                 isValidForm: false
                                             });
        
        registerModel.bind("change", function(e) {
            registerModel.set("isValidForm", checkIsValidForm());
        });
        
        var checkIsValidForm = function() {
            if (registerModel.Username != '' && registerModel.Password != '' && registerModel.ConfirmPassword != '' && registerModel.Password === registerModel.ConfirmPassword && registerModel.termsAccepted) {
                return true;
            } else {                
                return false;
            }
        };
        
        // Register user after required fields (username and password) are validated in Backend Services
        var register = function () {
            app.startLoading();
            app.Paypal.subscribeWithPayPal(function() {
                //success
                app.everlive.Users.register(
                    registerModel.Username,
                    registerModel.Password,
                    registerModel)
                    .then(function () {
                        console.log('Registration successful');
                        
                        // Authenticate using the username and password
                        app.everlive.Users.login(registerModel.Username, registerModel.Password, function(data) {
                            console.log('Logged in user: '+JSON.stringify(data));
                            app.userId = data.result.principal_id;
                            app.everlive.Users.currentUser()
                                .then(function (data) {
                                    console.log('app.set currentUser');
                                    app.currentUser = data.result;
                                    app.hideLoader();
                                    app.mobileApp.navigate('views/campaignsView.html');
                                },
                                function(error){
                                    app.hideLoader();
                                    alert(JSON.stringify(error));
                                });
                            
                        }, function(error) {
                            console.log('Logging error: '+JSON.stringify(error));
                            app.hideLoader();
                            alert(error.message);
                        });
                    },
                    function (err) {
                        console.log('Registration failed to save user: '+JSON.stringify(err));
                        app.hideLoader();
                        alert(err.message);
                        //app.showError(err.message);
                    });
            }, function () {
                // paypal failure
                app.hideLoader();
                alert('Conection with PayPal failed');
            });
        };
        
        var changeGroup = function () {
            if (registerModel.selectedGroup === 'mail') {
                this.set('model.isMail', true);
                this.set('model.isPayment', false);
            } else {
                this.set('model.isMail', false);
                this.set('model.isPayment', true);
            }
        };
        
        return {
            register: register,
            changeGroup: changeGroup,
            model: registerModel
        };
    }());
    
    return registerViewModel;
}());