/**
 * history view model
 */

var app = app || {};

app.History = (function (kendo) {
    var historyVM = function() {
        this.yearsModel = {
            id: 'Id',
            fields: {
                Year: {
                    field: 'Year',
                    defaultValue: ''
                }
            }
        };
        
        this.yearsDataSource = new kendo.data.DataSource({
            type: 'everlive',
            schema: {
                model: this.yearsModel
            },
            transport: {
                // Required by Backend Services
                typeName: 'Year'
            },
            schema: {
                parse: function (response) {
                    $.each(response.Result, function (index, year) {
                        year.hasItems = true;
                        year.openDetails = this.openDetails
                    });
                    return response;
                }
            },
            change: function (e) {

            },
            sort: { field: 'Year', dir: 'desc' }
        });
        
        this.openDetails = function (e) {
            var s = "";
        };
        
        this.weeksDataSource = [];
        
        var viewModel = kendo.observable(this);

        return viewModel;
    }
    
    var model = new historyVM();

    function showView(e) {
        var parentID = e.view.params.parent,
            element = e.view.element,
            backButton = element.find('#history-back'),
            listView = element.find("#historyList").data('kendoMobileListView');

        if (parentID) {

            var filterWeeks = {
                "YearId" : parentID
            };
            
            var data = app.everlive.data("Week");
            data.get(filterWeeks).then(function (data) {
                var weeks = $.map(data.result, function (week) {
                    return week.Id;
                });
                var historyData = app.everlive.data("History");
                var query = new Everlive.Query();
                query.where().isin('WeekId', weeks);
                query.expand({
                    "CampaignId": true
                });
                historyData.get(query).then(function (historyData) {
                    var campaigns = [];
                    $.each(historyData.result, function (index, campaign) {
                        var item = {};
                        item.hasItems = false;
                        item.NrVotes = campaign.NrVotes;
                        item.Amount = campaign.Amount;
                        item.Campaign = campaign.CampaignId;
                        
                        campaigns.push(item);
                    });
                    
                    console.log(parentID);
                    model.set('weeksDataSource', campaigns);
                });
            });
            
            backButton.show();
            //navBar.title("Weeks");
            listView.setDataSource(model.get('weeksDataSource'));
        } else {
            backButton.hide();
            //navBar.title('Years');
            listView.setDataSource(model.get('yearsDataSource'));
            //historyVM.get('yearsDataSource').read();
        }

        e.view.scroller.scrollTo(0, 0);
    }
    
    return {
        model: model,
        show: showView
    };
}(kendo));
