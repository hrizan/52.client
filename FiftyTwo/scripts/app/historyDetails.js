/**
 * history view model
 */

var app = app || {};

app.HistoryDetails = (function (kendo) {
    var historyVM = function() {
        
        this.historyDetailsDataSource = [];
        
        var viewModel = kendo.observable(this);

        return viewModel;
    }
    
    var model = new historyVM();

    function showView(e) {
        var parentID = e.view.params.parent,
            element = e.view.element,
            backButton = element.find('#history-back');

        if (parentID) {

            var filterWeeks = {
                "YearId" : parentID
            };
            
            var data = app.everlive.data("Week");
            data.get(filterWeeks).then(function (data) {
                var weeks = $.map(data.result, function (week) {
                    return week.Id;
                });
                var historyData = app.everlive.data("History");
                var query = new Everlive.Query();
                query.where().isin('WeekId', weeks);
                query.expand({
                    "CampaignId": true,
                    "WeekId": true
                });
                
                historyData.get(query).then(function (historyData) {
                    var campaigns = [];
                    $.each(historyData.result, function (index, campaign) {
                        var item = {};
                        item.hasItems = false;
                        item.NrVotes = campaign.NrVotes;
                        item.Amount = campaign.Amount;
                        item.Campaign = campaign.CampaignId;
                        item.Week = campaign.WeekId.Week;
                        item.getPictureSmall = function () {
                            if (campaign.CampaignId.PictureSmall) {
                                return app.AppHelper.resolvePictureUrl(campaign.CampaignId.PictureSmall);
                            }
                        }();
                        
                        campaigns.push(item);
                    });
                    
                    console.log(parentID);
                    model.set('historyDetailsDataSource', campaigns);
                });
            });
            
            backButton.show();

        }
    }
    
    return {
        model: model,
        show: showView
    };
}(kendo));