/**
 * Campaign details view model
 */

var app = app || {};

app.Campaign = (function () {
    'use strict'
    var campaignVM = function () {
        this.campaign = {};
        
        this.addVote =function (e) {          
            var campaign = this.campaign;
            var data = app.everlive.data('Campaign');
            var userId = app.userId;
            
             if(app.currentUser.HasVoted) {
                alert('You already voted this week');
                 return;
            }
            
            app.startLoading();
            this.set('campaign.NrVotes', campaign.NrVotes + 1);
            data.updateSingle({ Id: campaign.Id, 'NrVotes': campaign.NrVotes + 1 },
                function(data){
                    app.everlive.Users.updateSingle({ 'Id': userId, 'HasVoted': true, 'CampaignId': campaign.Id },
                    function(data){
                        app.Campaigns.showRefresh = true;
                        app.currentUser.HasVoted = true;
                        app.currentUser.CampaignId = campaign.Id;
                        $('#heart').addClass('campaign-details-heart-logo-voted');
                        app.hideLoader();
                        openDetails(data);
                    },
                    function(error){
                        app.hideLoader();
                        app.showError(error.message);
                        app.mobileApp.navigate('views/appEntrance.html');
                    });
                },
                function(error){
                    app.hideLoader();
                    app.showError(error.message);
                    app.mobileApp.navigate('views/appEntrance.html');
                });              
        };
        
        function openDetails(data) {
                $("#votedModal").data("kendoMobileModalView").open();
            }
        
        function checkSimulator(){
            if (window.navigator.simulator === true) {
                alert('This plugin is not available in the simulator.');
                return true;
            } else if (window.plugins === undefined || window.plugins.socialsharing === undefined) {
                alert('Plugin not found. Maybe you are running in AppBuilder Companion app which currently does not support this plugin.');
                return true;
            } else {
                return false;
            }
        };
        
        var viewModel = kendo.observable(this);

        return viewModel;
    };
    
    var model = new campaignVM();
    
    var campaignViewModel = (function () {
        var campaignUid,
            campaign;
        
        var init = function () {
        };
        
        var show = function (e) {
            campaignUid = e.view.params.uid;
            // Get current campaign (based on item uid) from Campaign model
            var data = app.everlive.data("Campaign");
            var filter = new Everlive.Query();
            filter.where().eq('Id', campaignUid);
            filter.expand({
                "OrganizationId" : true
            });
            
            data.get(filter)
                .then(function(data) {
                    campaign = data.result[0];
                    campaign.GetPictureBig = function () {
                        if (campaign.PictureBig) {
                            return app.AppHelper.resolvePictureUrl(campaign.PictureBig);
                        }
                    }
                    
                    if(app.currentUser && app.currentUser.HasVoted && campaign.Id === app.currentUser.CampaignId){
                        $('#heart').removeClass('campaign-details-heart-logo');
                        $('#heart').addClass('campaign-details-heart-logo-voted');
                     } else{
                        $('#heart').removeClass('campaign-details-heart-logo-voted');
                        $('#heart').addClass('campaign-details-heart-logo');
                    }
                    
                    model.set('campaign', campaign);
                },
              function(error) {
                  app.showError(error.message);
              });            
        };           

        
        return {
            init: init,
            show: show,
            model: model
        };
    }());
    
    return campaignViewModel;
}());