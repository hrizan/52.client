Psychologists Elizabeth Dunn, Lara Aknin and Michael Norton of Harvard Business School, did a research. Participants were given either $5 or $20 to spend by the end of the day. Half were instructed to buy something for themselves; the others used it help out somebody else. That evening, people who had been assigned to spend the money on someone else reported happier moods over the course of the day than did those people assigned to spend the money on themselves.

Have you ever wanted to donate but you didn’t know where to start?

The main idea of 52 application is to collect small but continuous donations on a weekly basis, hence the name - 52 weeks in a year. Every user commits to donate 1$ or 1€ weekly. This
makes a small difference to someone who donates but it makes a whole lot of difference to the one who receives.

We decided to use PayPal API that is mostly available worldwide, making donations possible and easy from all over the word. Using a back office portal 52 will collect and coordinate money 
transfers with charity foundations.

Our goal is to help people to donate money in a simple way. We aim to get weekly donations which would in summary be of great help to those who need it most. 

And we want to give a chance to our users to vote for their favorite campaign of that week. The weekly winner receives all the donations from that week.

Joining 52 family, the sum is greater than the parts. Your part is a single click. 

How hard can it be?